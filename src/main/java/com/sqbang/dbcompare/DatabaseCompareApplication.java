package com.sqbang.dbcompare;

import com.sqbang.dbcompare.listener.StartListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 启动类
 * @author suqiongbang
 * @date 2020/12/28 20:27
 */
@SpringBootApplication
public class DatabaseCompareApplication {

    public static void main(String[] args) {
        SpringApplication sa = new SpringApplication(DatabaseCompareApplication.class);
        sa.addListeners(new StartListener());
        sa.run(args);
//        SpringApplication.run(DatabaseCompareApplication.class, args);
    }
}
