package com.sqbang.dbcompare.util;

import com.sqbang.dbcompare.constant.CommonConst;
import com.sqbang.dbcompare.constant.Whether;
import com.sqbang.dbcompare.constant.enums.FieldTypeEnum;
import com.sqbang.dbcompare.pojo.bo.FieldBo;
import lombok.extern.log4j.Log4j2;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * 字段工具类
 *
 * @author suqiongbang
 * @date 2021/12/14 11:13
 */
@Log4j2
public class FieldUtil {
    /**
     * 将从mysql中查询获取到的字段描述文本转换为 TableField 实例中的变量
     *
     * @param field
     * @param typeFromSqlOfLowerCase
     */
    public static void handleTableField4Type(FieldBo field, String typeFromSqlOfLowerCase) {
//        log.info("处理表字段sql -> " + typeFromSqlOfLowerCase);

        field.setIsUnsigned(typeFromSqlOfLowerCase.contains(CommonConst.WORD_UNSIGNED) ?  Whether.YES : Whether.NO);
        typeFromSqlOfLowerCase = typeFromSqlOfLowerCase.replace(CommonConst.WORD_UNSIGNED, "").trim();

        // 字段类型，mysql类型
        String typeMetaStr;
        // 字段类型，mysql类型
        String lengthMetaStr = null;
        // 字段类型，mysql类型
        String secondLengthMetaStr = null;
        // 枚举值列表
        List<String> enumValueList = null;


        // 是否存在括号？
        if (typeFromSqlOfLowerCase.contains(CommonConst.SYMBOL_BRACKETS_LEFT)) {
            int position = typeFromSqlOfLowerCase.indexOf(CommonConst.SYMBOL_BRACKETS_LEFT);
            typeMetaStr = typeFromSqlOfLowerCase.substring(0, position);
            lengthMetaStr = typeFromSqlOfLowerCase.substring(position + 1);
            lengthMetaStr = lengthMetaStr.substring(0, lengthMetaStr.length() - 1);

            if (lengthMetaStr.contains(CommonConst.WORD_ENUM)) {
                String text = lengthMetaStr.replace(CommonConst.WORD_ENUM, "").replace(CommonConst.SYMBOL_BRACKETS_LEFT, "").replace(CommonConst.SYMBOL_BRACKETS_RIGHT, "");
                String[] split = text.split(CommonConst.SYMBOL_COMMA);
                enumValueList = new ArrayList<>();
                for (String s : split) {
                    enumValueList.add(s.replace("'", ""));
                }
            } else {
                // 如果存在逗号，表示数据有精度
                String[] array = lengthMetaStr.split(CommonConst.SYMBOL_COMMA);
                lengthMetaStr = array[0];
                lengthMetaStr = lengthMetaStr.replace(CommonConst.SYMBOL_BRACKETS_RIGHT, "");
                if (array.length == 2) {
                    secondLengthMetaStr = array[1];
                    secondLengthMetaStr = secondLengthMetaStr.replace(CommonConst.SYMBOL_BRACKETS_RIGHT, "");
                }
            }
        } else {
            typeMetaStr = typeFromSqlOfLowerCase;
        }

//        log.info("lengthMetaStr=" + lengthMetaStr);
//        log.info("secondLengthMetaStr=" + secondLengthMetaStr);

        field.setFieldTypeEnum(FieldTypeEnum.valueOf(typeMetaStr.toUpperCase(Locale.ROOT)));
        if (null != enumValueList) {
            field.setEnumValueList(enumValueList);
        } else {
            try {
                if (!typeMetaStr.startsWith(FieldTypeEnum.ENUM.name().toLowerCase(Locale.ROOT))) {
                    if (lengthMetaStr != null && lengthMetaStr.indexOf(CommonConst.SYMBOL_SPACE) > 0){
                        lengthMetaStr = lengthMetaStr.split(CommonConst.SYMBOL_SPACE)[0];
                    }
                    field.setLength(null != lengthMetaStr ? Integer.parseInt(lengthMetaStr) : 0);
                    field.setSecondLength(null != secondLengthMetaStr ? Integer.parseInt(secondLengthMetaStr) : 0);
                }
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 赋值字段
     *
     * @param field
     * @param isNull
     */
    public static void handleTableField4Null(FieldBo field, String isNull) {
        switch (isNull) {
            case "YES":
                field.setIsNotNull(Whether.NO);
                break;
            case "NO":
                field.setIsNotNull(Whether.YES);
                break;
            default: break;
        }
    }

    /**
     * 赋值字段
     *
     * @param field
     * @param type
     */
    public static void handleTableField4AutoIncr(FieldBo field, String type) {
        if (type.contains(CommonConst.MySql.DDL_AUTO_INCR2)) {
            field.setIsAutoIncr(1);
        } else {
            field.setIsAutoIncr(0);
        }
    }
}
