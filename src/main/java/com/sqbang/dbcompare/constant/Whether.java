package com.sqbang.dbcompare.constant;

/**
 * 是 or 否
 * @author suqiongbang
 * @date 2021/10/19 19:15
 */
public interface Whether {

    /**
     * 是
     */
    Integer YES = 1;

    /**
     * 否
     */
    Integer NO = 0;
}
