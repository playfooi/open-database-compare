package com.sqbang.dbcompare.constant;

/**
 * 系统常量
 * @author suqiongbang
 * @date 2021/10/19 19:15
 */
public interface SystemConstant {

    interface FilePath {

        /**
         * 程序使用的数据在磁盘的目录
         */
        String DATA_DISK_DIR = "database-compare-data";

        /**
         * 数据连接信息保存后的文件名
         */
        String DATABASE_INFO_DATA_FILE_NAME = "databaseInfoList.data";

        /**
         * 忽略单词拼写的文件名
         */
        String IGNORE_SPELLING_WORDS_DATA_FILE_NAME = "ignoreSpellingWords.data";

        /**
         * 忽略单词复数的文件名
         */
        String IGNORE_PLURALIZE_WORDS_DATA_FILE_NAME = "ignorePluralizeWords.data";

    }
}
