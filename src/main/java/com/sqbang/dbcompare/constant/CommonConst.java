package com.sqbang.dbcompare.constant;

/**
 * 通用常量
 *
 * @author suqiongbang
 * @date 2021/12/14 11:12
 */
public interface CommonConst {
    // 数据库连接前缀
    String URL_PREFIX = "jdbc:mysql://";
    // 数据库连接后缀
    // TODO: 2021/11/2 需要根据数据库的版本进行选择
    // 5.6以前的版本
//    String URL_SUFFIX = "?useUnicode=true&characterEncoding=utf-8&useSSL=true&serverTimezone=UTC&allowMultiQueries=true&autoReconnect=true&failOverReadOnly=false";
    // 5.6以后
    String URL_SUFFIX = "?useUnicode=true&characterEncodeing=UTF-8&useSSL=false&serverTimezone=GMT";

    // TODO: 2021/11/2 需要根据数据库的版本进行选择
    String DRIVER_CLASS_NAME = "com.mysql.cj.jdbc.Driver";
//    String DRIVER_CLASS_NAME = "com.mysql.jdbc.Driver";

    // 英文冒号
    String SYMBOL_COLON = ":";
    // 英文逗号
    String SYMBOL_COMMA = ",";
    // 英文单引号
    String SYMBOL_S_Q = "'";
    // 英文分号
    String SYMBOL_SEMICOLON = ";";
    // 左斜杠
    String SYMBOL_SLANTING_BAR = "/";
    // 左括号
    String SYMBOL_BRACKETS_LEFT = "(";
    // 右括号
    String SYMBOL_BRACKETS_RIGHT = ")";
    // 英文下划线
    String SYMBOL_UNDERLINE = "_";
    // 英文点号
    String SYMBOL_DOT = ".";
    // 英文井号
    String SYMBOL_NUMBER_SIGN = "#";
    // 英文空格
    String SYMBOL_SPACE = " ";
    // 英文反引号
    String SYMBOL_BACK_QUOTE = "`";

    // 单词 unsigned
    String WORD_UNSIGNED = "unsigned";
    // 单词 PRIMARY
    String WORD_PRIMARY = "PRIMARY";
    // 单词 enum
    String WORD_ENUM = "enum";


    interface MySql {
        // 查询库下所有的表名称
        String QRY_SHOW_TABLES = "show tables";
        // 查询数据库表的注释 替换为 数据库名称
        String QRY_SHOW_TABLE_COMMENT = "select table_name,table_comment from information_schema.TABLES where table_schema = '%s' ";

        // 查询表结构的sql语法，后面加上表名即可
        String QRY_DESCRIBE = "show full columns from ";
        // 查询表的索引sql语句，后面加上表名即可
        String QRY_SHOW_INDEX = "show index from ";
        // 查询表结构的建表sql
        String QRY_SHOW_CREATE_TABLE = "show create table ";




        // DDL

        // 创建一个全新字段
        String DDL_BLANKET = " ";
        String DDL_ALTER_TABLE = "alter table ";
        String DDL_ADD = " add ";
        String DDL_MODIFY = " modify ";
        String DDL_AFTER = " after ";
        String DDL_FIRST = " first";
        String DDL_NOT = " not ";
        String DDL_NULL = " null ";
        String DDL_COMMENT = " comment ";
        String DDL_AUTO_INCR = " auto_increment ";
        String DDL_AUTO_INCR2 = "auto_increment";
        String DDL_UNSIGNED = " unsigned ";
        String DDL_DEFAULT = " default ";
    }
}
