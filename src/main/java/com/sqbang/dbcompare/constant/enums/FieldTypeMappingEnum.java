package com.sqbang.dbcompare.constant.enums;

/**
 * 字段枚举类型
 *
 * @author suqiongbang
 * @date 2021/12/14 11:13
 */
public enum FieldTypeMappingEnum {
    BIGINT("BIGINT", "Long", ""),
    BINARY("BINARY", "String", ""),
    BIT("BIT", "Integer", ""),
    BLOB("BLOB", "String", ""),
    CHAR("CHAR", "String", ""),
    DATE("DATE", "LocalDate", "java.time.LocalDate"),
    DATETIME("DATETIME", "LocalDateTime", "java.time.LocalDateTime"),
    DECIMAL("DECIMAL", "BigDecimal", "java.math.BigDecimal"),
    DOUBLE("DOUBLE", "BigDecimal", "java.math.BigDecimal"),
    ENUM("ENUM", "String", ""),
    FLOAT("FLOAT", "BigDecimal", "java.math.BigDecimal"),
    GEOMETRY("GEOMETRY", "String", ""),
    GEOMETRYCOLLECTION("GEOMETRYCOLLECTION", "String", ""),
    INT("INT", "Integer", ""),
    INTEGER("INTEGER", "Integer", ""),
    JSON("JSON", "String", ""),
    LINESTRING("LINESTRING", "String", ""),
    LONGBLOB("LONGBLOB", "String", ""),
    LONGTEXT("LONGTEXT", "String", ""),
    MEDIUMBLOB("MEDIUMBLOB", "String", ""),
    MEDIUMINT("MEDIUMINT", "String", ""),
    MEDIUMTEXT("MEDIUMTEXT", "String", ""),
    MULTILINESTRING("MULTILINESTRING", "String", ""),
    MULTIPOINT("MULTIPOINT", "String", ""),
    NUMERIC("NUMERIC", "String", ""),
    POINT("POINT", "String", ""),
    POLYGON("POLYGON", "String", ""),
    REAL("REAL", "String", ""),
    SET("SET", "String", ""),
    SMALLINT("SMALLINT", "Integer", ""),
    TEXT("TEXT", "String", ""),
    TIME("TIME", "LocalTime", "java.time.LocalTime"),
    TIMESTAMP("TIMESTAMP", "Long", ""),
    TINYBLOB("TINYBLOB", "String", ""),
    TINYINT("TINYINT", "Integer", ""),
    TINYTEXT("TINYTEXT", "String", ""),
    VARBINARY("VARBINARY", "String", ""),
    VARCHAR("VARCHAR", "String", ""),
    YEAR("YEAR", "String", ""),
    ;

    /**
     * 数据库字段类型
     */
    private String code;

    /**
     * java字段类型
     */
    private String mapping;

    /**
     * java字段类型需要导入的包
     */
    private String packagePath;

    FieldTypeMappingEnum(String code, String mapping, String packagePath) {

        this.code = code;
        this.mapping = mapping;
        this.packagePath = packagePath;
    }

    public String getCode() {
        return code;
    }

    public String getMapping() {
        return mapping;
    }

    public String getPackagePath() {
        return packagePath;
    }

    public static FieldTypeMappingEnum of(String code) {
        for (FieldTypeMappingEnum value : FieldTypeMappingEnum.values()) {
            if (value.code.equals(code)) {
                return value;
            }
        }
        return VARCHAR;
    }

    public static String getMappingByCode(String code) {
        for (FieldTypeMappingEnum value : FieldTypeMappingEnum.values()) {
            if (value.code.equals(code)) {
                return value.mapping;
            }
        }
        return "";
    }
}
