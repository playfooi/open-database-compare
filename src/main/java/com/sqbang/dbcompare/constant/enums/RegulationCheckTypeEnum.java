package com.sqbang.dbcompare.constant.enums;

/**
 * 规范检查条项 枚举
 *
 * @author suqiongbang
 * @date 2021/12/14 11:14
 */
public enum RegulationCheckTypeEnum {

    /**
     * 检查小写
     */
    NAME_CONTAIN_UPPER_CASE("11", "应当使用小写"),

    /**
     * 检查单词拼写
     */
    NAME_ERROR_SPELL("12", "单词拼写建议，原词：%s，建议词：%s"),

    /**
     * 检查数字开头
     */
    NAME_DIGIT_START("13", "不能数字开头，原词：%s"),

    /**
     * 检查保留字
     */
    NAME_USE_KEYWORD("14", "禁用保留字"),

    /**
     * 检查复数单词
     */
    TABLE_NAME_CONTAIN_PLURALITY("31", "表名不要使用复数名词，原词：%s，建议词：%s"),

    /**
     * 检查必备字段
     */
    TABLE_MISS_FIELD("32", "表缺少必备三字段：id, create_time, update_time。"),

    /**
     * 检查索引命名。主键索引名为 pk_字段名、唯一索引名为 uk_字段名、普通索引名则为 idx_字段名
     */
    INDEX_NAME_ERROR("33", "索引命名不规范，原索引名：%s"),

    /**
     * 检查is开头
     */
    FIELD_IS_START_ERROR_TYPE("41", "字段是is开头，但类型不是unsigned tinyint"),

    /**
     * 检查is开头
     */
    FIELD_IS_START_ERROR_COMMENT("42", "字段是is开头，但字段备注没有包含“是否”二字"),

    /**
     * 检查“是否”类型的字段
     */
    FIELD_IS_CONTAIN_COMMENT("43", "字段备注包含“是否”二字，但字段名称不是is开头"),

    /**
     * 检查小数类型
     */
    FIELD_TYPE_USE_FLOAT("44", "小数类型为 decimal，禁止使用 float 和 double"),

    ;
    private String code;
    private String desc;

    RegulationCheckTypeEnum(String code, String desc) {

        this.code = code;
        this.desc = desc;
    }

    public String getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

    public static String getDescByCode(String code) {
        for (RegulationCheckTypeEnum value : RegulationCheckTypeEnum.values()) {
            if (value.code.equals(code)) {
                return value.desc;
            }
        }
        return "";
    }

    public static RegulationCheckTypeEnum getEnumByCode(String code) {
        for (RegulationCheckTypeEnum value : RegulationCheckTypeEnum.values()) {
            if (value.code.equals(code)) {
                return value;
            }
        }
        return null;
    }
}
