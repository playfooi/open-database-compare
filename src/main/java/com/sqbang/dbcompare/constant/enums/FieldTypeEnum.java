package com.sqbang.dbcompare.constant.enums;

/**
 * 字段枚举类型
 *
 * @author suqiongbang
 * @date 2021/12/14 11:13
 */
public enum FieldTypeEnum {
    BIGINT,
    BINARY,
    BIT,
    BLOB,
    CHAR,
    DATE,
    DATETIME,
    DECIMAL,
    DOUBLE,
    ENUM,
    FLOAT,
    GEOMETRY,
    GEOMETRYCOLLECTION,
    INT,
    INTEGER,
    JSON,
    LINESTRING,
    LONGBLOB,
    LONGTEXT,
    MEDIUMBLOB,
    MEDIUMINT,
    MEDIUMTEXT,
    MULTILINESTRING,
    MULTIPOINT,
    NUMERIC,
    POINT,
    POLYGON,
    REAL,
    SET,
    SMALLINT,
    TEXT,
    TIME,
    TIMESTAMP,
    TINYBLOB,
    TINYINT,
    TINYTEXT,
    VARBINARY,
    VARCHAR,
    YEAR,
    ;
}
