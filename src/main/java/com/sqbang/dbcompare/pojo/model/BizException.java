package com.sqbang.dbcompare.pojo.model;


import org.springframework.http.HttpStatus;

/**
 * 业务异常类
 * @author suqiongbang
 * @date 2021/10/19 22:58
 */
public class BizException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    private String msg;
    private int code;
    /**
     * 是否打印堆栈，默认打印
     */
    private boolean printStackTrace = true;

    public BizException() {

    }

    public BizException(HttpStatus resp) {
        super(resp.getReasonPhrase());
        this.code = resp.value();
        this.msg = resp.getReasonPhrase();
    }

    /**
     * 把格式化的msg，补充上详细信息
     *
     * @param resp        response code
     * @param description 详细信息
     */
    public BizException(HttpStatus resp, String description) {
        super(description);
        this.code = resp.value();
        this.msg = description;
    }

    public BizException(int code, String msg) {
        super(msg);
        this.msg = msg;
        this.code = code;
    }

    public BizException(int code, String msg, boolean printStackTrace) {
        super(msg);
        this.msg = msg;
        this.code = code;
        this.printStackTrace = printStackTrace;
    }

    public BizException(String msg, int code, Throwable e) {
        super(msg, e);
        this.msg = msg;
        this.code = code;
    }

    public boolean isPrintStackTrace() {
        return printStackTrace;
    }

    public void setPrintStackTrace(boolean printStackTrace) {
        this.printStackTrace = printStackTrace;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }


}