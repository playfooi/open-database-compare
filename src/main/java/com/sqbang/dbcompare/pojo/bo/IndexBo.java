package com.sqbang.dbcompare.pojo.bo;

import lombok.Data;

/**
 * 索引对象，字段注释来源：https://www.cnblogs.com/alazalazalaz/p/4083696.html
 * https://blog.csdn.net/jiayoudangdang/article/details/130387723
 * @author suqiongbang
 * @date 2021/11/12 22:46
 */
@Data
public class IndexBo {

    /**
     * 1.归属表名
     */
    private String table;

    /**
     * 2.如果索引不能包括重复词，则为0。如果可以，则为1。
     */
    private Integer nonUnique;

    /**
     * 3.索引的名称
     */
    private String keyName;

    /**
     * 4.索引中的列的序号。对于组合索引，这表示列在索引中的位置。
     */
    private Integer seqInIndex;

    /**
     * 5.作用于列名称
     */
    private String columnName;

    /**
     * 8.索引的前缀长度。对于部分索引，这表示索引的前缀长度。
     */
    private Integer subPart;

    /**
     * 11.用过的索引方法（BTREE, FULLTEXT, HASH, RTREE）
     */
    private String indexType;

    /**
     * 13.索引的注释
     */
    private String indexComment = "";
    /**
     * 是否双方都有(用于比对)
     */
    private Integer isBothHas = 0;
}
