package com.sqbang.dbcompare.pojo.bo;

import lombok.Data;

import java.util.LinkedHashMap;

/**
 * 数据库表信息对象
 * @author suqiongbang
 * @date 2020/12/28 20:51
 */
@Data
public class TableBo {

    /**
     * 表名
     */
    private String tableName;

    /**
     * 注释
     */
    private String comment;

    /**
     * 字段名模型
     */
    private LinkedHashMap<String, FieldBo> fieldMap;

    /**
     * 索引模型
     */
    private LinkedHashMap<String, IndexBo> indexMap;

    /**
     * 是否双方都有(用于比对)
     */
    private Integer isBothHas = 0;

}
