package com.sqbang.dbcompare.pojo.bo;

import com.sqbang.dbcompare.constant.enums.FieldTypeEnum;
import lombok.Data;

import java.util.List;

/**
 * 数据库表字段信息对象
 * @author suqiongbang
 * @date 2021/10/25 19:51
 */
@Data
public class FieldBo {
    /**
     * 字段名
     */
    private String name;
    /**
     * 字段类型
     */
    private FieldTypeEnum fieldTypeEnum;
    /**
     * 字段长度，可以为空
     */
    private Integer length;
    /**
     * 字段精度
     */
    private Integer secondLength;
    /**
     * 默认值
     */
    private String defaultValue;
    /**
     * 枚举值列表
     */
    private List<String> enumValueList;
    /**
     * 备注
     */
    private String comment = "";
    /**
     * 是否非空
     */
    private Integer isNotNull;
    /**
     * 是否自增
     */
    private Integer isAutoIncr;
    /**
     * 是否唯一
     */
    private Integer isUnique;
    /**
     * 是否主键
     */
    private Integer isPrimaryKey;

    /**
     * 是否无符号
     */
    private Integer isUnsigned;

    /**
     * 是否双方都有(用于比对)
     */
    private Integer isBothHas = 0;

}
