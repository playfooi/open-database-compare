package com.sqbang.dbcompare.pojo.bo;

import com.sqbang.dbcompare.constant.Whether;
import lombok.Data;

import java.util.List;

/**
 * 规范检查报告 业务对象
 * @author suqiongbang
 * @date 2021/11/3 21:05
 */
@Data
public class CheckReportBo {

    /**
     * 表名或字段名
     */
    private String name;

    /**
     * 建议列表
     */
    List<Suggest> suggestList;

    /**
     * 字段列表
     */
    List<CheckReportBo> childList;

    public CheckReportBo(){}

    public CheckReportBo(String name, List<Suggest> suggestList) {
        this.name = name;
        this.suggestList = suggestList;
    }

    @Data
    public static class Suggest {

        /**
         * 规范检查类型code
         */
        private String code;

        /**
         * 检查建议
         */
        private String desc;

        /**
         * 原词
         */
        private String originWord = "";

        /**
         * 是否展示，1是(默认)，0否
         */
        private Integer isShow = Whether.YES;

        public Suggest(){}
        public Suggest(String code, String desc){
            this.code = code;
            this.desc = desc;
        }
        public Suggest(String code, String desc, String originWord){
            this.code = code;
            this.desc = desc;
            this.originWord = originWord;
        }
    }
}
