package com.sqbang.dbcompare.pojo.bo;

import lombok.Data;

import java.util.List;

/**
 * 表信息
 *
 * @author suqiongbang
 * @date 2023/02/21 21:13
 */
@Data
public class TableInfoBo {

    /**
     * 数据库表名
     */
    private String tableName;

    /**
     * 表名-驼峰
     */
    private String tableCamelName;

    /**
     * 表名-首字母大写
     */
    private String tableUpperCaseName;

    /**
     * 表注释
     */
    private String tableComment;

    /**
     * 表字段信息
     */
    private List<FieldInfoBo> fieldList;


    @Data
    public static class FieldInfoBo {

        /**
         * 数据库表名
         */
        private String fieldName;

        /**
         * 表名-驼峰
         */
        private String fieldCamelName;

        /**
         * 是否是主键
         */
        private Integer isPrimaryKey;

        /**
         * 字段值类型
         */
        private String fieldValueType;

        /**
         * 字段值类型的包
         */
        private String fieldValuePackage;

        /**
         * 字段注释
         */
        private String fieldComment;
    }
}
