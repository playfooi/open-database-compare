package com.sqbang.dbcompare.pojo.bo;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;


/**
 * 差异报告 业务对象
 * @author suqiongbang
 * @date 2021/10/22 19:29
 */
@Data
public class DifferentReportBo {

    /**
     * 增加的表
     */
    private List<String> extraTableList = new ArrayList<>();

    /**
     * 缺少的表
     */
    private List<String> missTableList = new ArrayList<>();

    /**
     * 变化的表
     */
    private List<TableInfo> changeTableList = new ArrayList<>();



    /**
     * 表信息变化的实体
     */
    @Data
    public static class TableInfo {
        /**
         * 表名称
         */
        private String tableName;

        /**
         * 增加的字段
         */
        private List<String> extraColumnList = new ArrayList<>();

        /**
         * 缺少的字段
         */
        private List<String> missColumnList = new ArrayList<>();

        /**
         * 增加的索引
         */
        private List<String> extraIndexList = new ArrayList<>();

        /**
         * 缺少的索引
         */
        private List<String> missIndexList = new ArrayList<>();

        /**
         * 是否改过表的描述
         */
        private Integer isChangeComment = 0;

        /**
         * 表的描述（滞后库里面的）
         */
        private String targetComment;

        /**
         * 表的描述（先遣库里面的）
         */
        private String changeComment;

        /**
         * 前端是否展开
         */
        private Integer isClose = 0;

        /**
         * 有改动的列
         */
        List<FieldInfo> columnInfoList;

        /**
         * 有改动的索引
         */
        List<IndexInfo> indexInfoList;

    }

    /**
     * 列信息变化的实体
     */
    @Data
    public static class FieldInfo {

        /**
         * 列名称
         */
        private String fieldName;

        /**
         * 类型是否改变
         */
        private Integer isChangeFieldType = 0;
        private String targetFieldType;
        private String changeFieldType;

        /**
         * 数据长度是否改变
         */
        private Integer isChangeLength = 0;
        private Integer targetLength;
        private Integer changeLength;

        /**
         * 小数位数是否改变
         */
        private Integer isChangeSecondLength = 0;
        private Integer targetSecondLength;
        private Integer changeSecondLength;

        /**
         * 默认值是否改变
         */
        private Integer isChangeDefaultValue = 0;
        private String targetDefaultValue;
        private String changeDefaultValue;

        /**
         * 注释是否改变
         */
        private Integer isChangeComment = 0;
        private String targetComment;
        private String changeComment;

        /**
         * 非空是否改变
         */
        private Integer isChangeNotNull = 0;
        private Integer targetNotNull;
        private Integer changeNotNull;

        /**
         * 无符号是否改变
         */
        private Integer isChangeUnsigned = 0;
        private Integer targetUnsigned;
        private Integer changeUnsigned;

    }

    /**
     * 索引信息变化的实体
     */
    @Data
    public static class IndexInfo {

        /**
         * 索引名称
         */
        private String keyName;

        /**
         * 索引唯一性 是否改变
         */
        private Integer isChangeNonUnique = 0;
        private Integer targetNonUnique;
        private Integer changeNonUnique;


        /**
         * 作用于列名称 是否改变
         */
        private Integer isChangeColumnName = 0;
        private String targetColumnName;
        private String changeColumnName;

        /**
         * 索引类型 是否改变
         */
        private Integer isChangeIndexType = 0;
        private String targetIndexType;
        private String changeIndexType;

        /**
         * 索引注释 是否改变
         */
        private Integer isChangeIndexComment = 0;
        private String targetIndexComment;
        private String changeIndexComment;

    }
}
