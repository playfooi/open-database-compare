package com.sqbang.dbcompare.pojo.bo;

import lombok.Data;

import java.util.LinkedHashMap;

/**
 * 数据库表简短信息
 * @author suqiongbang
 * @date 2023/02/21 20:51
 */
@Data
public class TableBriefBo {

    /**
     * 表名
     */
    private String tableName;

    /**
     * 注释
     */
    private String comment;

}
