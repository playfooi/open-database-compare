package com.sqbang.dbcompare.pojo.vo;

import lombok.Data;

/**
 * 数据库连接信息 值对象
 *
 * @author suqiongbang
 * @date 2021/12/14 11:13
 */
@Data
public class DatabaseInfoVo {

    /**
     * 关键字，list的下标
     */
    private Integer key;

    /**
     * 连接名称
     */
    private String name;

    /**
     * 主机
     */
    private String host;

    /**
     * 端口
     */
    private Integer port;

    /**
     * 用户名
     */
    private String userName;

    /**
     * 数据库密码
     */
    private String password;

    /**
     * 数据库名称
     */
    private String database;
}
