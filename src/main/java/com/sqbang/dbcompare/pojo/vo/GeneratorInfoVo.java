package com.sqbang.dbcompare.pojo.vo;

import lombok.Data;

/**
 * 生成代码配置 值对象
 *
 * @author suqiongbang
 * @date 2021/12/14 11:13
 */
@Data
public class GeneratorInfoVo {

    /**
     * 数据库key
     */
    private Integer databaseKey;

    /**
     * 表名，支持*匹配
     */
    private String tableName;

    /**
     * 忽略表名前缀
     */
    private String ignoreTablePrefix;

    /**
     * 生成的文件磁盘路径，默认 jar 运行的目录
     */
    private String filePath;

    /**
     * 是否生成 Entity 文件
     */
    private Boolean isGenEntity;

    /**
     * 是否生成 Mapper 文件
     */
    private Boolean isGenMapper;

    /**
     * 是否生成 MapperXml 文件
     */
    private Boolean isGenMapperXml;

    /**
     * 是否生成 Service 文件
     */
    private Boolean isGenService;

    /**
     * 是否生成 ServiceImpl 文件
     */
    private Boolean isGenServiceImpl;

    /**
     * 是否生成 Controller 文件
     */
    private Boolean isGenController;

    /**
     * entity的包名
     */
    private String packageEntityName;

    /**
     * mapper的包名
     */
    private String packageMapperName;

    /**
     * mapperXml的包名
     */
    private String packageMapperXmlName;

    /**
     * service的包名
     */
    private String packageServiceName;

    /**
     * serviceImpl的包名
     */
    private String packageServiceImplName;

    /**
     * controller的包名
     */
    private String packageControllerName;
}
