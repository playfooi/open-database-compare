package com.sqbang.dbcompare.pojo.cache;

import com.sqbang.dbcompare.pojo.dto.DatabaseInfoDto;

import java.util.ArrayList;
import java.util.List;

/**
 * 用来存储数据
 * @author suqiongbang
 * @date 2021/10/19 22:11
 */
public class CommonData {

    /**
     * 数据库连接信息列表
     */
    public static List<DatabaseInfoDto> databaseInfoList = new ArrayList<>();

    /**
     * 忽略拼写检查的单词
     */
    public static String ignoreSpellingWordStr = "";

    /**
     * 忽略复数的单词
     */
    public static String ignorePluralizeWordStr = "";
}
