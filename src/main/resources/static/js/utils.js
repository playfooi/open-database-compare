let baseUrl = "http://localhost:8280";

/**
 * 获取浏览器地址栏参数值
 * @param {Object} name URL地址栏里面的参数名
 */
function getUrlParam(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
    var r = window.location.search.substr(1).match(reg);
    if (r != null) return (r[2]); return "";
}

/**
 * 对象赋值
 * @param {Object} sourceObj 原对象
 * @param {Object} targetObj 需要赋值的目标对象
 * @param {Object} isResetNull 如果目标对象在原对象中没有该属性，是否重置值为空
 */
function copyProperties(sourceObj, targetObj, isResetNull){
	if (!sourceObj && isResetNull) {
		targetObj = null;
	} else if (sourceObj && targetObj) {
		for (let key in targetObj) {
			if (targetObj[key] instanceof Object){
				copyProperties(sourceObj[key], targetObj[key], isResetNull);
			} else {
				if (sourceObj[key]) {
					targetObj[key] = sourceObj[key];
				} else if (isResetNull) {
					targetObj[key] = null;
				}
			}
		}
	}
}