# 数据库结构对比 open-database-compare

版本：v 4.1

#### 一、功能介绍
1. 数据库差异报告  
    数据库表结构差异比较，用于开发环境和生产环境的数据库结构比较来检查疏漏。
2. 数据库差异修正SQL  
    数据库结构发生变动之后，可生成差异SQL，用于补齐滞后数据库上的变化，通常结合功能1来验证是否需要执行补齐SQL；数据库数据发生变动时，也可以生成差异sql，用于补齐滞后库的数据
4. 数据库规范检查  
    根据《阿里巴巴Java开发手册》里面数据库规范部分进行检查，额外还增加了单词拼写检查。并且可以按需勾选检查项。
5. 数据库逆向生成 entity 实体类、mapper、service、controller 代码文件

#### 二、系统效果图
![Image text](https://gitee.com/sqbang/open-database-compare/raw/master/photo-v3-01.png)

![Image text](https://gitee.com/sqbang/open-database-compare/raw/master/photo-v2-02.png)

![Image text](https://gitee.com/sqbang/open-database-compare/raw/master/photo-v3-02.png)

![Image text](https://gitee.com/sqbang/open-database-compare/raw/master/photo-v2-03.png)

![Image text](https://gitee.com/sqbang/open-database-compare/raw/master/photo-v2-04.png)

![Image text](https://gitee.com/sqbang/open-database-compare/raw/master/photo-v2-05.png)

#### 三、软件架构

springboot + vue

#### 四、运行教程

方式一：直接运行
1. 在代码目录里面有个打包好jar（名字是database-compare-mysqlxxx.jar），直接点击下载（可根据Mysql版本下载不同的jar）。
2. 进入到磁盘下载目录，在此路径下按住键盘shift键，然后右键鼠标，点击菜单上的“在此处打开命令行窗口”
3. 执行：java -jar database-compare[Tab键]【可以按键盘Tab键可快速填充文件名】
4. 打开浏览器访问此地址：http://localhost:8280

方式二：编译运行（建议使用此方式，代码会比较新）
1. git clone https://gitee.com/sqbang/open-database-compare.git
3. 这是个springboot的maven工程，可以导入到idea/eclipse里面，直接跑起来。或者依次执行maven命令：clean -> compile -> package 打包之后在跑起来
4. 进入到工程的target磁盘目录中，在该目录打开命令行窗口，执行：java -jar database-compare.jar【可以按Tab键快速填充文件名】
5. 打开浏览器访问此地址：http://localhost:8280

#### 五、使用说明
1.新增数据库连接信息
2.分别填入数据库连接信息里面的key，即可使用

#### 六、鸣谢
1. 生成数据库差异SQL：https://gitee.com/bravof/differ
2. 单词拼写检查：https://github.com/houbb/word-checker/blob/master/README_ZH.md

#### 七、版本日志
##### v1.0 [2020年12月]
1. 新增数据库差异报告功能

##### v2.0 [2022年1月]
1. 新增生成数据库结构差异sql
2. 新增数据库规范检查功能
3. 调整v1.0版本的前端页面由webpack打包改为普通html

##### v3.0 [2023年3月]
1. 新增逆向生成java实体类功能

##### v4.0 [2023年7月]
1. 新增生成数据库表数据的差异sql

##### v4.1 [2023年9月]
1. 完善差异sql的索引差异